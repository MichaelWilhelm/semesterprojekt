﻿namespace Admin
{
    partial class frm_Bearbeiten
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Weiter = new System.Windows.Forms.Button();
            this.btn_Abbrechen = new System.Windows.Forms.Button();
            this.lbl_Marke = new System.Windows.Forms.Label();
            this.lbl_Stoffe = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.tbx_Stoffe = new System.Windows.Forms.TextBox();
            this.tbx_Marke = new System.Windows.Forms.TextBox();
            this.tbx_Name = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_Weiter
            // 
            this.btn_Weiter.Location = new System.Drawing.Point(129, 125);
            this.btn_Weiter.Name = "btn_Weiter";
            this.btn_Weiter.Size = new System.Drawing.Size(119, 37);
            this.btn_Weiter.TabIndex = 11;
            this.btn_Weiter.Text = "Weiter";
            this.btn_Weiter.UseVisualStyleBackColor = true;
            this.btn_Weiter.Click += new System.EventHandler(this.btn_Weiter_Click);
            // 
            // btn_Abbrechen
            // 
            this.btn_Abbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Abbrechen.Location = new System.Drawing.Point(259, 125);
            this.btn_Abbrechen.Name = "btn_Abbrechen";
            this.btn_Abbrechen.Size = new System.Drawing.Size(119, 37);
            this.btn_Abbrechen.TabIndex = 12;
            this.btn_Abbrechen.Text = "Abbrechen";
            this.btn_Abbrechen.UseVisualStyleBackColor = true;
            // 
            // lbl_Marke
            // 
            this.lbl_Marke.AutoSize = true;
            this.lbl_Marke.Location = new System.Drawing.Point(25, 54);
            this.lbl_Marke.Name = "lbl_Marke";
            this.lbl_Marke.Size = new System.Drawing.Size(51, 17);
            this.lbl_Marke.TabIndex = 15;
            this.lbl_Marke.Text = "Marke:";
            // 
            // lbl_Stoffe
            // 
            this.lbl_Stoffe.AutoSize = true;
            this.lbl_Stoffe.Location = new System.Drawing.Point(25, 82);
            this.lbl_Stoffe.Name = "lbl_Stoffe";
            this.lbl_Stoffe.Size = new System.Drawing.Size(49, 17);
            this.lbl_Stoffe.TabIndex = 14;
            this.lbl_Stoffe.Text = "Stoffe:";
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(25, 26);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(86, 17);
            this.lbl_Name.TabIndex = 13;
            this.lbl_Name.Text = "Artikelname:";
            // 
            // tbx_Stoffe
            // 
            this.tbx_Stoffe.Location = new System.Drawing.Point(129, 82);
            this.tbx_Stoffe.Name = "tbx_Stoffe";
            this.tbx_Stoffe.Size = new System.Drawing.Size(249, 22);
            this.tbx_Stoffe.TabIndex = 10;
            // 
            // tbx_Marke
            // 
            this.tbx_Marke.Location = new System.Drawing.Point(129, 54);
            this.tbx_Marke.Name = "tbx_Marke";
            this.tbx_Marke.Size = new System.Drawing.Size(249, 22);
            this.tbx_Marke.TabIndex = 9;
            // 
            // tbx_Name
            // 
            this.tbx_Name.Location = new System.Drawing.Point(129, 26);
            this.tbx_Name.Name = "tbx_Name";
            this.tbx_Name.Size = new System.Drawing.Size(249, 22);
            this.tbx_Name.TabIndex = 8;
            // 
            // frm_Bearbeiten
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Abbrechen;
            this.ClientSize = new System.Drawing.Size(414, 201);
            this.Controls.Add(this.btn_Weiter);
            this.Controls.Add(this.btn_Abbrechen);
            this.Controls.Add(this.lbl_Marke);
            this.Controls.Add(this.lbl_Stoffe);
            this.Controls.Add(this.lbl_Name);
            this.Controls.Add(this.tbx_Stoffe);
            this.Controls.Add(this.tbx_Marke);
            this.Controls.Add(this.tbx_Name);
            this.Name = "frm_Bearbeiten";
            this.Text = "Admin - Bearbeiten";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Weiter;
        private System.Windows.Forms.Button btn_Abbrechen;
        private System.Windows.Forms.Label lbl_Marke;
        private System.Windows.Forms.Label lbl_Stoffe;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.TextBox tbx_Stoffe;
        private System.Windows.Forms.TextBox tbx_Marke;
        private System.Windows.Forms.TextBox tbx_Name;
    }
}