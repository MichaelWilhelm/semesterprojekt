﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class frm_Bestellungen : Form
    {
        List<cls_Bestellungen> bestellungen_liste = new List<cls_Bestellungen>();
        public frm_Bestellungen()
        {
            InitializeComponent();
        }
        public void DatenAnzeigen()
        {
            bestellungen_liste.Clear();
            lbx_Bestellungen.Items.Clear();
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            string query = "SELECT * FROM `tbl_bestellungen` WHERE `Lieferdatum` = '" + "" + "'";

            MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
            cmdDB.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = cmdDB.ExecuteReader();
                //Daten extrahieren
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cls_Bestellungen bestellungen = new cls_Bestellungen(Convert.ToInt32(reader.GetString(0)), Convert.ToInt32(reader.GetString(1)), Convert.ToInt32(reader.GetString(2)), reader.GetString(3), reader.GetString(4));
                        bestellungen_liste.Add(bestellungen);
                        lbx_Bestellungen.Items.Add(bestellungen.Anzeige);
                    }
                }
                databaseConnection.Close();
            }
            catch
            {
                MessageBox.Show("Error", "WARNUNG");
            }
        }

        private void frm_Bestellungen_Load(object sender, EventArgs e)
        {
            DatenAnzeigen();
        }

        private void btn_Geliefert_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            string query = "UPDATE tbl_bestellungen SET `Lieferdatum` = '" +  DateTime.Now + "' WHERE `Bestellung_ID` = '" + bestellungen_liste[lbx_Bestellungen.SelectedIndex].BestellungID + "'";

            MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
            cmdDB.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = cmdDB.ExecuteReader();
                databaseConnection.Close();
                DatenAnzeigen();
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btn_AlleAnzeigen_Click(object sender, EventArgs e)
        {
            bestellungen_liste.Clear();
            lbx_Bestellungen.Items.Clear();
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            string query = "SELECT * FROM `tbl_bestellungen`";

            MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
            cmdDB.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = cmdDB.ExecuteReader();
                //Daten extrahieren
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cls_Bestellungen bestellungen = new cls_Bestellungen(Convert.ToInt32(reader.GetString(0)), Convert.ToInt32(reader.GetString(1)), Convert.ToInt32(reader.GetString(2)), reader.GetString(3), reader.GetString(4));
                        bestellungen_liste.Add(bestellungen);
                        lbx_Bestellungen.Items.Add(bestellungen.Anzeige);
                    }
                }
                databaseConnection.Close();
            }
            catch
            {
                MessageBox.Show("Error", "WARNUNG");
            }
        }
    }
}
