﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class frm_Wiederholen : Form
    {
        string m_artikelname;
        public frm_Wiederholen(string artikelname)
        {
            InitializeComponent();
            m_artikelname = artikelname;
        }

        private void btn_Ja_Click(object sender, EventArgs e)
        {
            frm_ArtikelHinzufuegenDetails dialog = new frm_ArtikelHinzufuegenDetails(m_artikelname);
            dialog.ShowDialog();
            this.Close();
        }
    }
}
