﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class frm_Lager : Form
    {
        List<cls_Daten> artikel_liste = new List<cls_Daten>();
        public frm_Lager()
        {
            InitializeComponent();
        }

        private void frm_Lager_Load(object sender, EventArgs e)
        {
            DatenAnzeigen();
        }
        public void DatenAnzeigen()
        {
            artikel_liste.Clear();
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            string query = "SELECT l.Artikelname, a.Marke, a.Stoffe, l.Groesse, l.Farbe, l.Menge, l.Auf_Lager, l.Lager_ID FROM `tbl_lager` l INNER JOIN `tbl_artikel` a ON (l.Artikelname = a.Artikelname)";

            MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
            cmdDB.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = cmdDB.ExecuteReader();
                //Daten extrahieren
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cls_Daten artikel = new cls_Daten(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), Convert.ToInt32(reader.GetString(5)), Convert.ToBoolean(reader.GetString(6)), Convert.ToInt32(reader.GetString(7)));
                        artikel_liste.Add(artikel);
                        lbx_Artikel.Items.Add(artikel.Anzeige);
                    }
                }
                databaseConnection.Close();
            }
            catch
            {
                MessageBox.Show("Error", "WARNUNG");
            }
        }

        private void btn_Bearbeiten_Click(object sender, EventArgs e)
        {
            if (lbx_Artikel.SelectedItems.Count == 1)
            {
                frm_Bearbeiten dialog = new frm_Bearbeiten(artikel_liste[lbx_Artikel.SelectedIndex]);
                dialog.ShowDialog();
                lbx_Artikel.Items.Clear();
                DatenAnzeigen();
            }
        }
        private void btn_Filtern_Click(object sender, EventArgs e)
        {
            lbx_Artikel.Items.Clear();
            Filtern();
        }
        public void Filtern()
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            string query;
            if (tbx_Marke.Text == "")
            {
                query = "SELECT l.Artikelname, a.Marke, a.Stoffe, l.Groesse, l.Farbe, l.Menge, l.Auf_Lager, l.Lager_ID FROM `tbl_lager` l INNER JOIN `tbl_artikel` a ON (l.Artikelname = a.Artikelname) WHERE l.Artikelname = '" + tbx_Artikelname.Text + "'";
            }
            else if (tbx_Artikelname.Text == "")
            {
                query = "SELECT l.Artikelname, a.Marke, a.Stoffe, l.Groesse, l.Farbe, l.Menge, l.Auf_Lager, l.Lager_ID FROM `tbl_lager` l INNER JOIN `tbl_artikel` a ON (l.Artikelname = a.Artikelname) WHERE a.Marke = '" + tbx_Marke.Text + "'";
            }
            else
                query = "SELECT l.Artikelname, a.Marke, a.Stoffe, l.Groesse, l.Farbe, l.Menge, l.Auf_Lager, l.Lager_ID FROM `tbl_lager` l INNER JOIN `tbl_artikel` a ON (l.Artikelname = a.Artikelname) WHERE a.Marke = '" + tbx_Marke.Text + "' AND l.Artikelname = '" + tbx_Artikelname.Text + "'";


            MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
            cmdDB.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();
                MySqlDataReader reader = cmdDB.ExecuteReader();
                //Daten extrahieren
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cls_Daten artikel = new cls_Daten(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), Convert.ToInt32(reader.GetString(5)), Convert.ToBoolean(reader.GetString(6)), Convert.ToInt32(reader.GetString(7)));
                        lbx_Artikel.Items.Add(artikel.Anzeige);
                    }
                }
                databaseConnection.Close();
            }
            catch
            {
                MessageBox.Show("Error", "WARNUNG");
            }
        }
    }
}
