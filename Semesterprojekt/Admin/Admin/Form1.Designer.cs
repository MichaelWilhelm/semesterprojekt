﻿namespace Admin
{
    partial class frm_Main
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Lager = new System.Windows.Forms.Button();
            this.btn_Hinzufuegen = new System.Windows.Forms.Button();
            this.btn_Bestellungen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Lager
            // 
            this.btn_Lager.Location = new System.Drawing.Point(12, 12);
            this.btn_Lager.Name = "btn_Lager";
            this.btn_Lager.Size = new System.Drawing.Size(267, 48);
            this.btn_Lager.TabIndex = 0;
            this.btn_Lager.Text = "Lager";
            this.btn_Lager.UseVisualStyleBackColor = true;
            this.btn_Lager.Click += new System.EventHandler(this.btn_lager_Click);
            // 
            // btn_Hinzufuegen
            // 
            this.btn_Hinzufuegen.Location = new System.Drawing.Point(12, 66);
            this.btn_Hinzufuegen.Name = "btn_Hinzufuegen";
            this.btn_Hinzufuegen.Size = new System.Drawing.Size(267, 48);
            this.btn_Hinzufuegen.TabIndex = 1;
            this.btn_Hinzufuegen.Text = "Artikel hinzufügen";
            this.btn_Hinzufuegen.UseVisualStyleBackColor = true;
            this.btn_Hinzufuegen.Click += new System.EventHandler(this.btn_hinzufuegen_Click);
            // 
            // btn_Bestellungen
            // 
            this.btn_Bestellungen.Location = new System.Drawing.Point(12, 120);
            this.btn_Bestellungen.Name = "btn_Bestellungen";
            this.btn_Bestellungen.Size = new System.Drawing.Size(267, 48);
            this.btn_Bestellungen.TabIndex = 2;
            this.btn_Bestellungen.Text = "Bestellungen";
            this.btn_Bestellungen.UseVisualStyleBackColor = true;
            this.btn_Bestellungen.Click += new System.EventHandler(this.btn_Bestellungen_Click);
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 181);
            this.Controls.Add(this.btn_Bestellungen);
            this.Controls.Add(this.btn_Hinzufuegen);
            this.Controls.Add(this.btn_Lager);
            this.Name = "frm_Main";
            this.Text = "Admin - Main";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Lager;
        private System.Windows.Forms.Button btn_Hinzufuegen;
        private System.Windows.Forms.Button btn_Bestellungen;
    }
}

