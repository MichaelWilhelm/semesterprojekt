﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class frm_BearbeitenDetails : Form
    {
        cls_Daten m_artikel;
        public cls_Daten Artikel
        {
            get
            {
                return m_artikel;
            }
            set
            {
                m_artikel = value;
            }
        }
        public frm_BearbeitenDetails()
        {
            InitializeComponent();
        }
        public frm_BearbeitenDetails(cls_Daten artikel)
        {
            InitializeComponent();
            Artikel = artikel;

            tbx_Farbe.Text = Artikel.Farbe;
            cbbx_Groesse.Text = Artikel.Groesse;
            num_Menge.Value = Artikel.Menge;
        }

        private void btn_Bearbeiten_Click(object sender, EventArgs e)
        {
            if (cbbx_Groesse.Text != "" && tbx_Farbe.Text != "")
            {
                string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);

                int lager = 0;
                if (num_Menge.Value > 0)
                {
                    lager = 1;
                }

                string query = "UPDATE tbl_lager SET `Groesse` = '" + cbbx_Groesse.Text + "', `Farbe` = '" + tbx_Farbe.Text + "', `Menge` = '" + num_Menge.Value + "', `Auf_Lager` = '" + lager + "' WHERE `Lager_ID` = '" + Artikel.LagerID + "'";

                MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
                cmdDB.CommandTimeout = 60;

                try
                {
                    databaseConnection.Open();
                    MySqlDataReader reader = cmdDB.ExecuteReader();
                    databaseConnection.Close();
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void frm_BearbeitenDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cbbx_Groesse.Text == "" || tbx_Farbe.Text == "")
            {
                MessageBox.Show("Sie müssen überall etwas eingeben!", "WARNUNG");
                frm_BearbeitenDetails dialog = new frm_BearbeitenDetails(m_artikel);
                dialog.ShowDialog();
            }
        }
    }
}
