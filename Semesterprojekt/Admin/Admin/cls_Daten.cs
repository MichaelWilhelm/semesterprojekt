﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin
{
    public class cls_Daten
    {
        string m_artikelname;
        string m_marke;
        string m_stoffe;
        string m_groesse;
        string m_farbe;
        int m_menge;
        bool m_aufLager;
        int m_lagerid;
        public cls_Daten(string artikelname, string marke, string stoffe, string groesse, string farbe, int menge, bool aufLager, int lagerid)
        {
            m_artikelname = artikelname;
            m_marke = marke;
            m_stoffe = stoffe;
            m_groesse = groesse;
            m_farbe = farbe;
            m_menge = menge;
            m_aufLager = aufLager;
            m_lagerid = lagerid;
        }
        public string Anzeige
        {
            get
            {
                return string.Format("{0} ----- {1} ----- {2} ----- {3} ----- {4} ----- {5} ----- {6}", m_marke, m_artikelname, m_stoffe, m_groesse, m_farbe, m_menge, m_aufLager);
            }
        }
        public string Artikelname
        {
            get { return m_artikelname; }
            set { m_artikelname = value; }
        }
        public string Marke
        {
            get { return m_marke; }
            set { m_marke = value; }
        }
        public string Stoffe
        {
            get { return m_stoffe; }
            set { m_stoffe = value; }
        }
        public string Groesse
        {
            get { return m_groesse; }
            set { m_groesse = value; }
        }
        public string Farbe
        {
            get { return m_farbe; }
            set { m_farbe = value; }
        }
        public int Menge
        {
            get { return m_menge; }
            set { m_menge = value; }
        }
        public bool AufLager
        {
            get { return m_aufLager; }
            set { m_aufLager = value; }
        }
        public int LagerID
        {
            get { return m_lagerid; }
            set { m_lagerid = value; }
        }
    }
}