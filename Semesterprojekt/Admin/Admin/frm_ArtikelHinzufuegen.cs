﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class frm_ArtikelHinzufuegen : Form
    {
        string m_artikelname;
        public frm_ArtikelHinzufuegen()
        {
            InitializeComponent();
        }
        private void btn_Weiter_Click(object sender, EventArgs e)
        {
            if (tbx_Name.Text != "" && tbx_Marke.Text != "" & tbx_Stoffe.Text != "")
            {
                string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);

                string query = "INSERT INTO `tbl_artikel`(`Artikelname`, `Marke`, `Stoffe`) VALUES ('" + tbx_Name.Text + "', '" + tbx_Marke.Text + "', '" + tbx_Stoffe.Text + "')";

                MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
                cmdDB.CommandTimeout = 60;

                try
                {
                    databaseConnection.Open();
                    MySqlDataReader reader = cmdDB.ExecuteReader();
                    m_artikelname = tbx_Name.Text;
                    frm_ArtikelHinzufuegenDetails dialog = new frm_ArtikelHinzufuegenDetails(m_artikelname);
                    dialog.ShowDialog();
                    databaseConnection.Close();
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Error", "WARNUNG");
                }
            }
            else
            {
                MessageBox.Show("Geben Sie bitte überall etwas ein!", "WARNUNG");
            }
        }
    }
}
