﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin
{
    public class cls_Bestellungen
    {
        int m_bestellungid;
        int m_lagerid;
        int m_menge;
        string m_bestelldatum;
        string m_lieferdatum;
        public cls_Bestellungen(int bestellungid, int lagerid, int menge, string bestelldatum, string lieferdatum)
        {
            m_bestellungid = bestellungid;
            m_lagerid = lagerid;
            m_menge = menge;
            m_bestelldatum = bestelldatum;
            m_lieferdatum = lieferdatum;
        }
        public string Anzeige
        {
            get
            {
                return string.Format("{0} ----- {1} ----- {2} ----- {3} ----- {4}", m_bestellungid, m_lagerid, m_menge, m_bestelldatum, m_lieferdatum);
            }
        }
        public int BestellungID
        {
            get { return m_bestellungid; }
            set { m_bestellungid = value; }
        }
        public int LagerID
        {
            get { return m_lagerid; }
            set { m_lagerid = value; }
        }
        public int Menge
        {
            get { return m_menge; }
            set { m_menge = value; }
        }
        public string Bestelldatum
        {
            get { return m_bestelldatum; }
            set { m_bestelldatum = value; }
        }
        public string Lieferdatum
        {
            get { return m_lieferdatum; }
            set { m_lieferdatum = value; }
        }
    }
}
