﻿namespace Admin
{
    partial class frm_ArtikelHinzufuegenDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Erstellen = new System.Windows.Forms.Button();
            this.btn_Abbrechen = new System.Windows.Forms.Button();
            this.lbl_Farbe = new System.Windows.Forms.Label();
            this.lbl_Menge = new System.Windows.Forms.Label();
            this.lbl_Groesse = new System.Windows.Forms.Label();
            this.tbx_Farbe = new System.Windows.Forms.TextBox();
            this.num_Menge = new System.Windows.Forms.NumericUpDown();
            this.cbbx_Groesse = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.num_Menge)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Erstellen
            // 
            this.btn_Erstellen.Location = new System.Drawing.Point(111, 126);
            this.btn_Erstellen.Name = "btn_Erstellen";
            this.btn_Erstellen.Size = new System.Drawing.Size(119, 37);
            this.btn_Erstellen.TabIndex = 3;
            this.btn_Erstellen.Text = "Erstellen";
            this.btn_Erstellen.UseVisualStyleBackColor = true;
            this.btn_Erstellen.Click += new System.EventHandler(this.btn_Erstellen_Click);
            // 
            // btn_Abbrechen
            // 
            this.btn_Abbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Abbrechen.Location = new System.Drawing.Point(241, 126);
            this.btn_Abbrechen.Name = "btn_Abbrechen";
            this.btn_Abbrechen.Size = new System.Drawing.Size(119, 37);
            this.btn_Abbrechen.TabIndex = 4;
            this.btn_Abbrechen.Text = "Abbrechen";
            this.btn_Abbrechen.UseVisualStyleBackColor = true;
            // 
            // lbl_Farbe
            // 
            this.lbl_Farbe.AutoSize = true;
            this.lbl_Farbe.Location = new System.Drawing.Point(19, 58);
            this.lbl_Farbe.Name = "lbl_Farbe";
            this.lbl_Farbe.Size = new System.Drawing.Size(49, 17);
            this.lbl_Farbe.TabIndex = 15;
            this.lbl_Farbe.Text = "Farbe:";
            // 
            // lbl_Menge
            // 
            this.lbl_Menge.AutoSize = true;
            this.lbl_Menge.Location = new System.Drawing.Point(19, 87);
            this.lbl_Menge.Name = "lbl_Menge";
            this.lbl_Menge.Size = new System.Drawing.Size(55, 17);
            this.lbl_Menge.TabIndex = 14;
            this.lbl_Menge.Text = "Menge:";
            // 
            // lbl_Groesse
            // 
            this.lbl_Groesse.AutoSize = true;
            this.lbl_Groesse.Location = new System.Drawing.Point(19, 30);
            this.lbl_Groesse.Name = "lbl_Groesse";
            this.lbl_Groesse.Size = new System.Drawing.Size(53, 17);
            this.lbl_Groesse.TabIndex = 13;
            this.lbl_Groesse.Text = "Größe:";
            // 
            // tbx_Farbe
            // 
            this.tbx_Farbe.Location = new System.Drawing.Point(111, 58);
            this.tbx_Farbe.Name = "tbx_Farbe";
            this.tbx_Farbe.Size = new System.Drawing.Size(249, 22);
            this.tbx_Farbe.TabIndex = 1;
            // 
            // num_Menge
            // 
            this.num_Menge.Location = new System.Drawing.Point(111, 87);
            this.num_Menge.Name = "num_Menge";
            this.num_Menge.Size = new System.Drawing.Size(120, 22);
            this.num_Menge.TabIndex = 2;
            // 
            // cbbx_Groesse
            // 
            this.cbbx_Groesse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbx_Groesse.FormattingEnabled = true;
            this.cbbx_Groesse.Items.AddRange(new object[] {
            "XXS",
            "XS",
            "S",
            "M",
            "L",
            "XL",
            "2XL",
            "3XL",
            "4XL",
            "5XL"});
            this.cbbx_Groesse.Location = new System.Drawing.Point(111, 30);
            this.cbbx_Groesse.Name = "cbbx_Groesse";
            this.cbbx_Groesse.Size = new System.Drawing.Size(121, 24);
            this.cbbx_Groesse.TabIndex = 0;
            // 
            // frm_ArtikelHinzufuegenDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Abbrechen;
            this.ClientSize = new System.Drawing.Size(381, 181);
            this.Controls.Add(this.cbbx_Groesse);
            this.Controls.Add(this.num_Menge);
            this.Controls.Add(this.btn_Erstellen);
            this.Controls.Add(this.btn_Abbrechen);
            this.Controls.Add(this.lbl_Farbe);
            this.Controls.Add(this.lbl_Menge);
            this.Controls.Add(this.lbl_Groesse);
            this.Controls.Add(this.tbx_Farbe);
            this.Name = "frm_ArtikelHinzufuegenDetails";
            this.Text = "Admin - Artikel hinzufügen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_ArtikelHinzufuegenDetails_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.num_Menge)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Erstellen;
        private System.Windows.Forms.Button btn_Abbrechen;
        private System.Windows.Forms.Label lbl_Farbe;
        private System.Windows.Forms.Label lbl_Menge;
        private System.Windows.Forms.Label lbl_Groesse;
        private System.Windows.Forms.TextBox tbx_Farbe;
        private System.Windows.Forms.NumericUpDown num_Menge;
        private System.Windows.Forms.ComboBox cbbx_Groesse;
    }
}