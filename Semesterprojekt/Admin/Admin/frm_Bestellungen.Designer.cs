﻿namespace Admin
{
    partial class frm_Bestellungen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbx_Bestellungen = new System.Windows.Forms.ListBox();
            this.btn_Zurueck = new System.Windows.Forms.Button();
            this.lbl_Beschriftung = new System.Windows.Forms.Label();
            this.btn_Geliefert = new System.Windows.Forms.Button();
            this.btn_AlleAnzeigen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbx_Bestellungen
            // 
            this.lbx_Bestellungen.FormattingEnabled = true;
            this.lbx_Bestellungen.ItemHeight = 16;
            this.lbx_Bestellungen.Location = new System.Drawing.Point(12, 33);
            this.lbx_Bestellungen.Name = "lbx_Bestellungen";
            this.lbx_Bestellungen.Size = new System.Drawing.Size(599, 276);
            this.lbx_Bestellungen.TabIndex = 0;
            // 
            // btn_Zurueck
            // 
            this.btn_Zurueck.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Zurueck.Location = new System.Drawing.Point(467, 315);
            this.btn_Zurueck.Name = "btn_Zurueck";
            this.btn_Zurueck.Size = new System.Drawing.Size(144, 46);
            this.btn_Zurueck.TabIndex = 1;
            this.btn_Zurueck.Text = "Zurück";
            this.btn_Zurueck.UseVisualStyleBackColor = true;
            // 
            // lbl_Beschriftung
            // 
            this.lbl_Beschriftung.AutoSize = true;
            this.lbl_Beschriftung.Location = new System.Drawing.Point(12, 13);
            this.lbl_Beschriftung.Name = "lbl_Beschriftung";
            this.lbl_Beschriftung.Size = new System.Drawing.Size(458, 17);
            this.lbl_Beschriftung.TabIndex = 2;
            this.lbl_Beschriftung.Text = "Bestellung-ID ---- Lager-ID ---- Menge ---- Bestelldatum ---- Lieferdatum";
            // 
            // btn_Geliefert
            // 
            this.btn_Geliefert.Location = new System.Drawing.Point(317, 315);
            this.btn_Geliefert.Name = "btn_Geliefert";
            this.btn_Geliefert.Size = new System.Drawing.Size(144, 46);
            this.btn_Geliefert.TabIndex = 3;
            this.btn_Geliefert.Text = "Geliefert";
            this.btn_Geliefert.UseVisualStyleBackColor = true;
            this.btn_Geliefert.Click += new System.EventHandler(this.btn_Geliefert_Click);
            // 
            // btn_AlleAnzeigen
            // 
            this.btn_AlleAnzeigen.Location = new System.Drawing.Point(167, 315);
            this.btn_AlleAnzeigen.Name = "btn_AlleAnzeigen";
            this.btn_AlleAnzeigen.Size = new System.Drawing.Size(144, 46);
            this.btn_AlleAnzeigen.TabIndex = 4;
            this.btn_AlleAnzeigen.Text = "Alle anzeigen";
            this.btn_AlleAnzeigen.UseVisualStyleBackColor = true;
            this.btn_AlleAnzeigen.Click += new System.EventHandler(this.btn_AlleAnzeigen_Click);
            // 
            // frm_Bestellungen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Zurueck;
            this.ClientSize = new System.Drawing.Size(620, 369);
            this.Controls.Add(this.btn_AlleAnzeigen);
            this.Controls.Add(this.btn_Geliefert);
            this.Controls.Add(this.lbl_Beschriftung);
            this.Controls.Add(this.btn_Zurueck);
            this.Controls.Add(this.lbx_Bestellungen);
            this.Name = "frm_Bestellungen";
            this.Text = "Admin - Bestellungen";
            this.Load += new System.EventHandler(this.frm_Bestellungen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbx_Bestellungen;
        private System.Windows.Forms.Button btn_Zurueck;
        private System.Windows.Forms.Label lbl_Beschriftung;
        private System.Windows.Forms.Button btn_Geliefert;
        private System.Windows.Forms.Button btn_AlleAnzeigen;
    }
}