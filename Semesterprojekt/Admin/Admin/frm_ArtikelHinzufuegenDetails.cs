﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class frm_ArtikelHinzufuegenDetails : Form
    {
        string m_artikelname;
        public frm_ArtikelHinzufuegenDetails(string artikelname)
        {
            InitializeComponent();
            m_artikelname = artikelname;
        }
        private void btn_Erstellen_Click(object sender, EventArgs e)
        {
            if (cbbx_Groesse.Text != "" && tbx_Farbe.Text != "")
            {
                string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                
                int lager = 0;
                if(num_Menge.Value > 0)
                {
                    lager = 1;
                }

                string query = "INSERT INTO `tbl_lager`(`Artikelname`, `Groesse`, `Farbe`, `Menge`, `Auf_Lager`) VALUES ('" + m_artikelname + "', '" + cbbx_Groesse.Text + "', '" + tbx_Farbe.Text + "', '" + num_Menge.Value + "', '" + lager + "')";

                MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
                cmdDB.CommandTimeout = 60;

                try
                {
                    databaseConnection.Open();
                    MySqlDataReader reader = cmdDB.ExecuteReader();
                    databaseConnection.Close();
                    frm_Wiederholen dialog = new frm_Wiederholen(m_artikelname);
                    dialog.ShowDialog();
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
            else
            {
                MessageBox.Show("Geben Sie bitte überall etwas ein!", "WARNUNG");
            }
        }

        private void frm_ArtikelHinzufuegenDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cbbx_Groesse.Text == "" || tbx_Farbe.Text == "")
            {
                MessageBox.Show("Sie müssen überall etwas eingeben!", "WARNUNG");
                frm_ArtikelHinzufuegenDetails dialog = new frm_ArtikelHinzufuegenDetails(m_artikelname);
                dialog.ShowDialog();
            }
        }
    }
}
