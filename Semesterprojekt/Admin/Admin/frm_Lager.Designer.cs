﻿namespace Admin
{
    partial class frm_Lager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Bearbeiten = new System.Windows.Forms.Button();
            this.btn_Zurueck = new System.Windows.Forms.Button();
            this.lbl_Beschriftung = new System.Windows.Forms.Label();
            this.lbx_Artikel = new System.Windows.Forms.ListBox();
            this.btn_Filtern = new System.Windows.Forms.Button();
            this.tbx_Marke = new System.Windows.Forms.TextBox();
            this.tbx_Artikelname = new System.Windows.Forms.TextBox();
            this.lbl_Artikelname = new System.Windows.Forms.Label();
            this.lbl_Marke = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Bearbeiten
            // 
            this.btn_Bearbeiten.Location = new System.Drawing.Point(454, 369);
            this.btn_Bearbeiten.Name = "btn_Bearbeiten";
            this.btn_Bearbeiten.Size = new System.Drawing.Size(164, 46);
            this.btn_Bearbeiten.TabIndex = 3;
            this.btn_Bearbeiten.Text = "Bearbeiten";
            this.btn_Bearbeiten.UseVisualStyleBackColor = true;
            this.btn_Bearbeiten.Click += new System.EventHandler(this.btn_Bearbeiten_Click);
            // 
            // btn_Zurueck
            // 
            this.btn_Zurueck.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Zurueck.Location = new System.Drawing.Point(624, 369);
            this.btn_Zurueck.Name = "btn_Zurueck";
            this.btn_Zurueck.Size = new System.Drawing.Size(164, 46);
            this.btn_Zurueck.TabIndex = 4;
            this.btn_Zurueck.Text = "Zurück";
            this.btn_Zurueck.UseVisualStyleBackColor = true;
            // 
            // lbl_Beschriftung
            // 
            this.lbl_Beschriftung.AutoSize = true;
            this.lbl_Beschriftung.Location = new System.Drawing.Point(12, 18);
            this.lbl_Beschriftung.Name = "lbl_Beschriftung";
            this.lbl_Beschriftung.Size = new System.Drawing.Size(508, 17);
            this.lbl_Beschriftung.TabIndex = 5;
            this.lbl_Beschriftung.Text = "Marke ---- Artikelname ---- Stoffe ---- Größe ---- Farbe ---- Menge ---- auf Lage" +
    "r";
            // 
            // lbx_Artikel
            // 
            this.lbx_Artikel.FormattingEnabled = true;
            this.lbx_Artikel.ItemHeight = 16;
            this.lbx_Artikel.Location = new System.Drawing.Point(12, 52);
            this.lbx_Artikel.Name = "lbx_Artikel";
            this.lbx_Artikel.Size = new System.Drawing.Size(775, 308);
            this.lbx_Artikel.TabIndex = 4;
            // 
            // btn_Filtern
            // 
            this.btn_Filtern.Location = new System.Drawing.Point(284, 369);
            this.btn_Filtern.Name = "btn_Filtern";
            this.btn_Filtern.Size = new System.Drawing.Size(164, 46);
            this.btn_Filtern.TabIndex = 2;
            this.btn_Filtern.Text = "Filtern";
            this.btn_Filtern.UseVisualStyleBackColor = true;
            this.btn_Filtern.Click += new System.EventHandler(this.btn_Filtern_Click);
            // 
            // tbx_Marke
            // 
            this.tbx_Marke.Location = new System.Drawing.Point(119, 381);
            this.tbx_Marke.Name = "tbx_Marke";
            this.tbx_Marke.Size = new System.Drawing.Size(100, 22);
            this.tbx_Marke.TabIndex = 0;
            // 
            // tbx_Artikelname
            // 
            this.tbx_Artikelname.Location = new System.Drawing.Point(119, 418);
            this.tbx_Artikelname.Name = "tbx_Artikelname";
            this.tbx_Artikelname.Size = new System.Drawing.Size(100, 22);
            this.tbx_Artikelname.TabIndex = 1;
            // 
            // lbl_Artikelname
            // 
            this.lbl_Artikelname.AutoSize = true;
            this.lbl_Artikelname.Location = new System.Drawing.Point(9, 418);
            this.lbl_Artikelname.Name = "lbl_Artikelname";
            this.lbl_Artikelname.Size = new System.Drawing.Size(86, 17);
            this.lbl_Artikelname.TabIndex = 11;
            this.lbl_Artikelname.Text = "Artikelname:";
            // 
            // lbl_Marke
            // 
            this.lbl_Marke.AutoSize = true;
            this.lbl_Marke.Location = new System.Drawing.Point(12, 381);
            this.lbl_Marke.Name = "lbl_Marke";
            this.lbl_Marke.Size = new System.Drawing.Size(51, 17);
            this.lbl_Marke.TabIndex = 12;
            this.lbl_Marke.Text = "Marke:";
            // 
            // frm_Lager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Zurueck;
            this.ClientSize = new System.Drawing.Size(800, 479);
            this.Controls.Add(this.lbl_Marke);
            this.Controls.Add(this.lbl_Artikelname);
            this.Controls.Add(this.tbx_Artikelname);
            this.Controls.Add(this.tbx_Marke);
            this.Controls.Add(this.btn_Filtern);
            this.Controls.Add(this.btn_Bearbeiten);
            this.Controls.Add(this.btn_Zurueck);
            this.Controls.Add(this.lbl_Beschriftung);
            this.Controls.Add(this.lbx_Artikel);
            this.Name = "frm_Lager";
            this.Text = "Admin - Lager";
            this.Load += new System.EventHandler(this.frm_Lager_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Bearbeiten;
        private System.Windows.Forms.Button btn_Zurueck;
        private System.Windows.Forms.Label lbl_Beschriftung;
        private System.Windows.Forms.ListBox lbx_Artikel;
        private System.Windows.Forms.Button btn_Filtern;
        private System.Windows.Forms.TextBox tbx_Marke;
        private System.Windows.Forms.TextBox tbx_Artikelname;
        private System.Windows.Forms.Label lbl_Artikelname;
        private System.Windows.Forms.Label lbl_Marke;
    }
}