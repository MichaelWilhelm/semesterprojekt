﻿namespace Admin
{
    partial class frm_Wiederholen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Text = new System.Windows.Forms.Label();
            this.btn_Ja = new System.Windows.Forms.Button();
            this.btn_Nein = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_Text
            // 
            this.lbl_Text.AutoSize = true;
            this.lbl_Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Text.Location = new System.Drawing.Point(25, 28);
            this.lbl_Text.Name = "lbl_Text";
            this.lbl_Text.Size = new System.Drawing.Size(378, 25);
            this.lbl_Text.TabIndex = 0;
            this.lbl_Text.Text = "Möchten Sie noch einen Eintrag erstellen?";
            // 
            // btn_Ja
            // 
            this.btn_Ja.Location = new System.Drawing.Point(30, 72);
            this.btn_Ja.Name = "btn_Ja";
            this.btn_Ja.Size = new System.Drawing.Size(172, 51);
            this.btn_Ja.TabIndex = 1;
            this.btn_Ja.Text = "Ja";
            this.btn_Ja.UseVisualStyleBackColor = true;
            this.btn_Ja.Click += new System.EventHandler(this.btn_Ja_Click);
            // 
            // btn_Nein
            // 
            this.btn_Nein.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Nein.Location = new System.Drawing.Point(231, 72);
            this.btn_Nein.Name = "btn_Nein";
            this.btn_Nein.Size = new System.Drawing.Size(172, 51);
            this.btn_Nein.TabIndex = 2;
            this.btn_Nein.Text = "Nein";
            this.btn_Nein.UseVisualStyleBackColor = true;
            // 
            // frm_Wiederholen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Nein;
            this.ClientSize = new System.Drawing.Size(444, 155);
            this.Controls.Add(this.btn_Nein);
            this.Controls.Add(this.btn_Ja);
            this.Controls.Add(this.lbl_Text);
            this.Name = "frm_Wiederholen";
            this.Text = "frm_Wiederholen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Text;
        private System.Windows.Forms.Button btn_Ja;
        private System.Windows.Forms.Button btn_Nein;
    }
}