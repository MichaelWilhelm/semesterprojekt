﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin
{
    public partial class frm_Bearbeiten : Form
    {
        cls_Daten m_artikel;
        public cls_Daten Artikel
        {
            get
            {
                return m_artikel;
            }
            set
            {
                m_artikel = value;
            }
        }
        public frm_Bearbeiten()
        {
            InitializeComponent();
        }
        public frm_Bearbeiten(cls_Daten artikel)
        {
            InitializeComponent();
            Artikel = artikel;

            tbx_Name.Text = Artikel.Artikelname;
            tbx_Marke.Text = Artikel.Marke;
            tbx_Stoffe.Text = Artikel.Stoffe;
        }

        private void btn_Weiter_Click(object sender, EventArgs e)
        {
            if (tbx_Name.Text != "" && tbx_Marke.Text != "" && tbx_Stoffe.Text != "")
            {
                string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=semesterprojekt_db;";
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);

                string query = "UPDATE tbl_artikel SET `Artikelname` = '" + tbx_Name.Text + "', `Marke` = '" + tbx_Marke.Text + "', `Stoffe` = '" + tbx_Stoffe.Text + "' WHERE `Artikelname` = '" + Artikel.Artikelname + "' ";

                MySqlCommand cmdDB = new MySqlCommand(query, databaseConnection);
                cmdDB.CommandTimeout = 60;

                try
                {
                    databaseConnection.Open();
                    MySqlDataReader reader = cmdDB.ExecuteReader();
                    databaseConnection.Close();
                    frm_BearbeitenDetails dialog = new frm_BearbeitenDetails(m_artikel);
                    dialog.ShowDialog();
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }
    }
}
