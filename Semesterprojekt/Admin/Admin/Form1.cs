﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin
{
    public partial class frm_Main : Form
    {
        public frm_Main()
        {
            InitializeComponent();
        }

        private void btn_lager_Click(object sender, EventArgs e)
        {
            frm_Lager lager = new frm_Lager();
            lager.ShowDialog();
        }

        private void btn_hinzufuegen_Click(object sender, EventArgs e)
        {
            frm_ArtikelHinzufuegen hinzufuegen = new frm_ArtikelHinzufuegen();
            hinzufuegen.ShowDialog();
        }

        private void btn_Bestellungen_Click(object sender, EventArgs e)
        {
            frm_Bestellungen bestellungen = new frm_Bestellungen();
            bestellungen.ShowDialog();
        }
    }
}
