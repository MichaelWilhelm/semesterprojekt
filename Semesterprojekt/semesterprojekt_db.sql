-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 26. Jan 2021 um 21:42
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `semesterprojekt_db`
--
CREATE DATABASE IF NOT EXISTS `semesterprojekt_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `semesterprojekt_db`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_artikel`
--

CREATE TABLE `tbl_artikel` (
  `Artikel_ID` int(11) NOT NULL,
  `Artikelname` varchar(255) NOT NULL,
  `Marke` varchar(255) NOT NULL,
  `Stoffe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_bestellungen`
--

CREATE TABLE `tbl_bestellungen` (
  `Bestellung_ID` int(11) NOT NULL,
  `Lager_ID` int(11) NOT NULL,
  `Menge` int(11) NOT NULL,
  `Bestelldatum` char(10) NOT NULL,
  `Lieferdatum` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `tbl_bestellungen`
--

INSERT INTO `tbl_bestellungen` (`Bestellung_ID`, `Lager_ID`, `Menge`, `Bestelldatum`, `Lieferdatum`) VALUES
(3, 1, 10, '', '25.01.2021'),
(5, 2, 10, '', '25.01.2021'),
(6, 1, 10, '', '26.01.2021');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_lager`
--

CREATE TABLE `tbl_lager` (
  `Lager_ID` int(11) NOT NULL,
  `Artikelname` varchar(50) NOT NULL,
  `Groesse` char(5) NOT NULL,
  `Farbe` varchar(30) NOT NULL,
  `Menge` int(11) NOT NULL,
  `Auf_Lager` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_artikel`
--
ALTER TABLE `tbl_artikel`
  ADD PRIMARY KEY (`Artikel_ID`);

--
-- Indizes für die Tabelle `tbl_bestellungen`
--
ALTER TABLE `tbl_bestellungen`
  ADD PRIMARY KEY (`Bestellung_ID`);

--
-- Indizes für die Tabelle `tbl_lager`
--
ALTER TABLE `tbl_lager`
  ADD PRIMARY KEY (`Lager_ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_artikel`
--
ALTER TABLE `tbl_artikel`
  MODIFY `Artikel_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT für Tabelle `tbl_bestellungen`
--
ALTER TABLE `tbl_bestellungen`
  MODIFY `Bestellung_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `tbl_lager`
--
ALTER TABLE `tbl_lager`
  MODIFY `Lager_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
